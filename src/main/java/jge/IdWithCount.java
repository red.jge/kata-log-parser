package jge;

public class IdWithCount {
    int id;
    int count;

    public IdWithCount(int id, int count) {
        this.id=id;
        this.count=count;
    }

    @Override
    public String toString(){
        return new StringBuilder("id[").append(id).append("] count[").append(count).append(']').toString();
    }

}
