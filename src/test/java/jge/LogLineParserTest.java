package jge;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class LogLineParserTest {

    static LogLineParser logLineParser;

    @BeforeAll
    static void beforeAll() {
        logLineParser = new LogLineParser();
    }

    @Test
    void testParseIdAsFirstParam() {
        Assertions.assertEquals(638,logLineParser.extractIdFromLine("http://a.com?id=638"));
    }
    @Test
    void testParseIdAsSecondParam() {
        Assertions.assertEquals(988,logLineParser.extractIdFromLine("https://test.net?a=1&id=988&b=2&c=3"));
        Assertions.assertEquals(1,logLineParser.extractIdFromLine("https://test.net?a=1&id=1&b=2&c=3"));
    }
    @Test
    void testParseIdLastParam() {
        Assertions.assertEquals(171,logLineParser.extractIdFromLine("http://a.com?id=171"));
        Assertions.assertEquals(10,logLineParser.extractIdFromLine("http://bid.org?a=1&id=10"));
        Assertions.assertEquals(567,logLineParser.extractIdFromLine("http://bid.org?a=1&id=567"));
        Assertions.assertEquals(1,logLineParser.extractIdFromLine("https://test.net?a=1&id=1&b=2&c=3"));
    }

    @Test
    void testParseIdEmpty() {
        Assertions.assertThrows(IllegalStateException.class, () -> logLineParser.extractIdFromLine("http://a.com?id="));
    }

    @Test
    void testParseIdNan() {
        Assertions.assertThrows(IllegalStateException.class, () -> logLineParser.extractIdFromLine("http://a.com?id="));
    }

    @Test
    void testParseNoIdToParse() {
        Assertions.assertThrows(IllegalStateException.class, () -> logLineParser.extractIdFromLine("https://test.net?a=1&b=2&c=3"));
    }

}